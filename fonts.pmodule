# Copyright 2020 Portmod Authors
# Distributed under the terms of the GNU General Public License
# Either version 3 of the License, or any later version

"""Configures custom true type fonts"""
import os
from fonts import (
    list_fonts,
    set_font,
    create_font_xml,
    get_selected_fonts,
    check_selected_fonts,
)

font_options = ["magic", "daedric", "mono"]


def do_update(state):
    # Recreate font xml
    selected = get_selected_fonts(state.ROOT)
    check_selected_fonts(state.ROOT, selected)
    create_font_xml(selected)


def do_set(state, args):
    """Set the desired font"""
    set_font(state.ROOT, args.font_type, args.font)


def describe_set_options():
    return ["font_type", "font"]


def describe_set_parameters():
    return [
        "The font type to list options for. One of daedric, magic or mono",
        "The index (as reported by the list command) or name of the font to select",
    ]


def do_list(state, args):
    """Lists available fonts"""
    list_fonts(args.font_type, state.ROOT)


def describe_list_options():
    return ["font_type"]


def describe_list_parameters():
    return ["The font type to list options for. One of daedric, magic or mono"]
