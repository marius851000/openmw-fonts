from distutils.core import setup

setup(
    name="openmw_fonts",
    version="0.4.0",
    description="OpenMW Font selector for Portmod",
    author="Benjamin Winger",
    author_email="bmw@disroot.org",
    url="https://gitlab.com/portmod/openmw-fonts",
    py_modules=["fonts"],
)
